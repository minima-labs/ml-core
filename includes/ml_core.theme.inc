<?php

/**
 * @file
 * Theming functions for the Core module.
 */

/**
 * Preprocess variables for theme_image_style.
 */
function ml_core_preprocess_image_style(&$variables) {
  if (isset($variables['style_name'])) {
    $variables['attributes']['class'][] = 'image--' . $variables['style_name'];

    if (in_array($variables['style_name'], array(
      'ml_one_half',
      'ml_one_third',
      'ml_two_thirds',
      'ml_one_quarter',
      'ml_three_quarters',
    ))) {
      $class = str_replace('_', '-', str_replace('ml_', 'desk--', $variables['style_name']));
      $variables['attributes']['class'][] = $class;
    }
  }
}

/**
 * Process variables for ml-core-slideshow.tpl.php
 */
function template_preprocess_ml_core_slideshow(&$variables) {
  $variables['attributes_array'] = array(
    'class' => array('ml-core-slideshow', 'cycle-slideshow', 'auto'),
    'data-cycle-fx' => $variables['settings']['effect'],
    'data-cycle-timeout' => $variables['settings']['timeout'],
    'data-cycle-slides' => '.ml-core-slideshow__slide',
    'data-cycle-pager' => '.ml-core-slideshow__pager',
    'data-cycle-pager-template' => '',
    'data-cycle-loader' => true,
    'data-cycle-progressive' => "#ml-core-slides",
    //'data-cycle-pause-on-hover' => 'true',
  );

  $variables['slides'] = array();

  foreach ($variables['items'] as $item) {
    $variables['slides'][] = theme('ml_core_slideshow_slide', array(
      'item' => $item,
      'settings' => $variables['settings'],
    ));
  }

  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('module', 'ml_core') . '/theme/js/ml_core.slideshow.js', array('scope' => 'footer'));
}

/**
 * Process variables for ml-core-slide.tpl.php
 */
function template_preprocess_ml_core_slideshow_slide(&$variables) {
  $variables['image'] = theme('image_style', array(
    'path' => $variables['item']['uri'],
    'alt' => $variables['item']['alt'],
    'width' => $variables['item']['width'],
    'height' => $variables['item']['height'],
    'style_name' => $variables['settings']['image_style'],
    'attributes' => array(
      'class' => array('ml-core-slideshow-slide__image'),
    ),
  ));

  $variables['title'] = $variables['item']['title'];
  $variables['description'] = $variables['item']['alt'];
}

/**
 * Theme callback for the refresh meta tag.
 */
function theme_metatag_refresh($variables) {
  $output = '';
  $element = &$variables['element'];

  element_set_attributes($element, array(
    '#name' => 'http-equiv',
    '#value' => 'content',
  ));

  unset($element['#value']);
  return theme('html_tag', $variables);
}
