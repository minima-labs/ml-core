<?php
/**
 * @file
 * ml_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ml_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_file__image';
  $strongarm->value = array(
    'view_modes' => array(
      'preview' => array(
        'custom_settings' => TRUE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'full_width' => array(
        'custom_settings' => TRUE,
      ),
      'one_half' => array(
        'custom_settings' => TRUE,
      ),
      'one_third' => array(
        'custom_settings' => TRUE,
      ),
      'two_thirds' => array(
        'custom_settings' => TRUE,
      ),
      'one_quarter' => array(
        'custom_settings' => TRUE,
      ),
      'three_quarters' => array(
        'custom_settings' => TRUE,
      ),
      'ml_thumbnail_large' => array(
        'custom_settings' => TRUE,
      ),
      'ml_thumbnail_small' => array(
        'custom_settings' => TRUE,
      ),
      'ml_full_width' => array(
        'custom_settings' => TRUE,
      ),
      'ml_one_half' => array(
        'custom_settings' => TRUE,
      ),
      'ml_one_third' => array(
        'custom_settings' => TRUE,
      ),
      'ml_two_thirds' => array(
        'custom_settings' => TRUE,
      ),
      'ml_one_quarter' => array(
        'custom_settings' => TRUE,
      ),
      'ml_three_quarters' => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'file' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'preview' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'teaser' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_file__image'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jquery_update_jquery_admin_version';
  $strongarm->value = '1.5';
  $export['jquery_update_jquery_admin_version'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'jquery_update_jquery_version';
  $strongarm->value = '1.8';
  $export['jquery_update_jquery_version'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_admin_theme';
  $strongarm->value = 1;
  $export['node_admin_theme'] = $strongarm;

  return $export;
}
