<div class="ml-core-slideshow__slide">
  <?php print $image; ?>

  <?php if ($title || $description): ?>
  <div class="ml-core-slideshow__caption">
    <div class="ml-core-slideshow__title"><?php print $title; ?></div>
    <div class="ml-core-slideshow__description"><?php print $description; ?></div>
  </div>
  <?php endif; ?>
</div>
