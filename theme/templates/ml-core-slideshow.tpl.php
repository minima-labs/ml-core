<div<?php print $attributes; ?>>
  <div class="ml-core-slideshow__progress"></div>
  <div class="ml-core-slideshow__pager">
    <?php foreach ($slides as $slide): ?>
      <span></span>
    <?php endforeach; ?>
  </div>

  <?php print array_shift($slides); ?>

  <script id="ml-core-slides" type="text/cycle" data-cycle-split="---">
    <?php foreach ($slides as $slide): ?>
      <?php print $slide; ?>
      ---
    <?php endforeach; ?>
  </script>
</div>
